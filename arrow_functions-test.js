const faker = require('faker');

faker.locale = 'pt_BR';

const Clients = [];

for (let index = 0; index < 500; index++) {
  const name = faker.name.findName();
  const birthday = faker.date.past(109, 2019);
  const genre = faker.random.boolean() == true ? 'M' : 'F';
  const lastpurchase = faker.date.past();
  const countpurchase = faker.random.number(50);

  client = {
    name,
    birthday,
    genre,
    lastpurchase,
    countpurchase,
  }
  Clients.push(client)
}

//--1-- PRONTO Desenvolva, utilizando filter , uma função que, dado um caractere de 
//entrada, retorne todos os registros de clientes cujo o nome inicia com o caractere.
let firstLetra = (pl) => Clients.filter(value => value.name[0] === pl)
console.log('Clientes com a letra inicial indicada')
console.log(firstLetra("F"))
console.log('_____________________________________________')


//--2-- PRONTO Retorne o array de clientes
const names = Clients.map(value => value)
console.log("Array de Clientes");
console.log(names)
console.log('_____________________________________________')


//--3-- PRONTO Desenvolva uma função que, dado o caractere de 
//entrada, retorna apenas um número com o total de registros encontrados.
const quantLetraInicial = (pl) => Clients.map(value => value.name[0] === pl)
  .reduce((acumulador, next) => (acumulador += next));
console.log("Quantidade de clientes com a letra inicial indicada");
console.log(quantLetraInicial("A"))
console.log('_____________________________________________')


//--4-- PRONTO Desenvolva uma função que retorne apenas os nomes dos clientes.
const clientes = Clients.map(value => value.name)
console.log(clientes)
console.log('_____________________________________________')


//--5-- PRONTO Desenvolva uma função que retorne apenas o primeiro nome dos clientes.
let firstName = Clients.map(value => value.name.split(" ")[0])
console.log('Primeiro nome');
console.log(firstName)
console.log('_____________________________________________')


//--6-- PENDENTE Desenvolva uma função que retorne apenas o primeiro nome dos clientes 
//cujo os nomes começam com o caractere de entrada da função.
//let letraUm = Clients.filter(value => value.name.split(" ")[0] == "F")
//console.log('Primeiro nome com o mesmo caractere de entrada')
//console.log(letraUm)
//console.log('_____________________________________________')


//--7-- Pendente Desenvolva uma função que retorne
//todos os usuários que são maiores de idade.
//const maiores = Clients.filter(value => value.birthday - 18 ? maiores : value)
//console.log(maiores)


//--8-- Pronto Desenvolva uma função que, dado um nome de entrada
//retorna se o nome está contido na lista.
let contem = (nome) => Clients.map(value => value.name.split(" ")[0]).includes(nome) ? ("O nome está na lista") : ("O nome não está na lista");
console.log('Verifica se o nome está na lista')
console.log(contem("Carlos"))
console.log('_____________________________________________')


//--9-- PRONTO Implemente uma função que retorna o
//total de vendas realizadas somando as compras de todos os clientes.
let somaCompras = Clients.map(value => value.countpurchase)
  .reduce((acumulador, next) => (acumulador += next));
console.log('Total de vendas')
console.log(somaCompras)
console.log('_____________________________________________')


//--10-- PENDENTE Implemente uma função que retorne os dados dos clientes que não compram há mais de 1 ano.
//let comprasMaisdeAno = Clients.filter(value => value.lastpurchase.split()[0, 4] + 1 != 2019 ? value : comprasMaisdeAno)
//console.log(comprasMaisdeAno)


//--11-- Pronto Implemente uma função que retorne os dados dos clientes que já realizaram mais de 15 compras.
let maisQuinze = Clients.filter(value => value.countpurchase > 48)
console.log('Clientes que compraram mais de 15X');
console.log(maisQuinze)